#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import time
import datetime
import requests
from bs4 import BeautifulSoup

siteUrl = 'http://eda.ru'

s = requests.Session()
s.headers.update({
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
})


def load_page_data(url, session):
    request = session.get(url)
    return request.text


def contain_recipe_data(text):
    soup = BeautifulSoup(text, 'html.parser')
    recipe = {}
    title = soup.find('meta', property="og:title")
    if title:
        recipe['name'] = soup.find('meta', property="og:title")["content"]
    recipe['url'] = recipeUrl  # soup.find("meta",  property="og:url")["content"]
    image = soup.find("meta", property="og:image")
    if image:
        recipe['images'] = [image["content"]]
    else:
        recipe['images'] = []
    recipe['tags'] = []
    recipe['cuisine_by_nationality'] = []
    recipe['recipe_type'] = []
    recipe['context'] = []
    description = soup.find("meta", property="og:description")
    if description:
        recipe['description'] = description["content"]
    recipe['recipe_date'] = ''
    recipe['cooking_time'] = ''
    recipe['preparation_time'] = ''
    recipe['skill'] = ''
    recipe['number_of_servings'] = 0
    recipe['ingredients'] = []
    for row in soup.find_all('tr', {'class': 'ingredient'}):
        cells = row.find_all('td')
        if len(cells) == 2:
            recipe['ingredients'].append({'name': cells[0].text, 'quantity': cells[1].text})
        else:
            recipe['ingredients'].append({'full': cells[0].text})
    recipe['step_by_step_recipe'] = []
    for instruction in soup.find_all('li', {'class': 'instruction'}):
        if len(instruction.div.contents) > 2:
            recipe['step_by_step_recipe'].append({'description': instruction.div.contents[2].strip()})
        else:
            recipe['step_by_step_recipe'].append({'description': instruction.div.contents[0].strip()})
    recipe['recipe'] = []
    recipe['recipe_images'] = []
    return recipe


serveCode = 200
pageIndex = 1
startTime = time.time()
while serveCode == 200:
    if pageIndex == 1:
        pageUrl = 'http://eda.ru/recepty'
    else:
        pageUrl = 'http://eda.ru/recepty/page' + str(pageIndex)
    request = s.get(pageUrl)
    serveCode = request.status_code
    if serveCode == 200:
        pageData = request.text
        soup = BeautifulSoup(pageData, 'html.parser')
        recipeLinks = soup.find_all('h3', {'class': 'b-recipe-widget__name'})
        for recipeLink in recipeLinks:
            recipeUrl = 'http://eda.ru' + recipeLink.find('a')['href']
            print recipeUrl
            recipeData = load_page_data(recipeUrl, s)
            print contain_recipe_data(recipeData)
    pageIndex += 1
# print 'Скрипт выполнялся: ' + str(time.time() - startTime) + ' сек.'
