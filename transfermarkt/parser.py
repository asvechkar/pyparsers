#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import requests
from bs4 import BeautifulSoup

siteUrl = "http://www.transfermarkt.co.uk/"

s = requests.Session()
s.headers.update({
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0"
})


def load_page_data(url, session):
    req = session.get(url)
    return req.text


def contain_team_data(text, index):
    soup = BeautifulSoup(text, "html.parser")
    team = {
        "ontoid": "ext_dtm_verein_" + str(index)
    }
    title = soup.find("meta", property="og:title")
    if title:
        team["Title"] = [
            {
                "value": title["content"],
                "langua": "en"
            }
        ]
    team["ids"] = [
        {"value": "http://www.transfermarkt.de/crystal-palace/startseite/verein/" + str(index), "type": "url"}]
    team["Participants"] = []
    selectPlayers = soup.find("select", {"id": "spieler_select_breadcrumb"})
    optgroups = selectPlayers.find_all("optgroup")
    for optgroup in optgroups:
        options = optgroup.find_all("option")
        for option in options:
            if option["value"] != "":
                team["Participants"].append(
                    {"value": "[[#ext_dtm_spieler_" + str(option["value"]) + "]]", "Role": ["CurrentPlayer@on"]})
    team["isa"] = {"otype": [{"subvalue": "SportTeam@on", "value": "Org"}]}

    return team


def contain_player_data(text, index):
    soup = BeautifulSoup(text, "html.parser")
    player = {
        "ontoid": "ext_dtm_spieler_" + str(index)
    }
    title = soup.find("meta", property="og:title")
    if title:
        player["Title"] = [
            {
                "value": title["content"],
                "langua": "en"
            }
        ]
    url = soup.find("meta", property="og:url")
    if url:
        url = url["content"]
    else:
        url = ""
    image = soup.find("meta", property="og:image")
    if image:
        image = image["content"]
    else:
        image = ""
    player["Image"] = [
        {
            "src": [
                {
                    "url": url
                }
            ],
            "value": image
        }
    ]
    height = soup.find("span", itemprop="height")
    player["params"] = {
        "Height": [
            {
                "value": height.string if height else "",
                "type": "m"
            }
        ]
    }
    birthPlace = soup.find("span", itemprop="birthPlace")
    player["InitPlace"] = [
        {
            "value": birthPlace.string if birthPlace else ""
        }
    ]
    birthDate = soup.find("span", itemprop="birthDate")
    player["InitDate"] = [
        {
            "value": birthDate.string.strip()[:12] if birthDate else ""
        }
    ]
    nationality = soup.find("span", itemprop="nationality")
    player["isa"] = {
        "otype": [{"value": "Hum"}],
        "tags": [{"value": "Sportsman@on"}, {"value": "Footballer@on"}],
        "Nationality": [
            {
                "value": nationality.string if nationality else ""
            }
        ],
        "FieldPosition": [
            {
                "value": "Abwehr - Linker Verteidiger",
                "langua": "de"
            }
        ]
    }
    return player

if len(sys.argv) > 1 and sys.argv[1] == 'team':
    # Получаем информацию о команде
    serveCode = 200
    pageIndex = 1
    startTime = time.time()
    for pageIndex in range(1, 4000):
        if pageIndex == 1:
            pageUrl = siteUrl + "real-madrid/startseite/verein/" + str(pageIndex)
        else:
            pageUrl = siteUrl + "real-madrid/startseite/verein/" + str(pageIndex)
        request = s.get(pageUrl)
        # print pageUrl
        serveCode = request.status_code
        if serveCode == 200:
            pageData = request.text
            print contain_team_data(pageData, pageIndex)
        pageIndex += 1
    # print "Скрипт выполнялся: " + str(time.time() - startTime) + " сек."
else:
    # Получаем информацию об игроке
    serveCode = 200
    pageIndex = 1
    startTime = time.time()
    for pageIndex in range(1, 40000):
        if pageIndex == 1:
            pageUrl = siteUrl + "marco-laping/profil/spieler/" + str(pageIndex)
        else:
            pageUrl = siteUrl + "marco-laping/profil/spieler/" + str(pageIndex)
        request = s.get(pageUrl)
        # print pageUrl
        serveCode = request.status_code
        if serveCode == 200:
            pageData = request.text
            print contain_player_data(pageData, pageIndex)
        pageIndex += 1
