import datetime
import csv
from urllib.request import urlopen
from html.parser import HTMLParser
import requests

#that is class for table of data
class DataTable:
    def __init__(self):
        self.rows = list()
        
    def addRow(self, dictRow):
        self.rows.append(dictRow.copy())
        
    def getRow(self, ind):
        return self.rows[ind]
            
    def getSize(self):
        return len(self.rows)
        
 # class of parser https://www.smithandcrown.com/icos/       
class MyHTMLParser(HTMLParser):
    def __init__(self, dataTable):
        HTMLParser.__init__(self)
        self.isStartDataTag = False
        self.isTagName = False
        self.isSpanName = False
        self.isDescTd = False
        self.isDescP = False
        self.isRaisedTd = False
        self.isRaisedComm = False
        self.isStartDate = False
        self.isStartDateComm = False
        self.isEndDateComm = False
        self.isEndDate = False
        self.dataTable = dataTable
        self.ind = 0
        self.row = dict()
        self.format1 = "%b %d, %Y" 
        self.format2 = "%H:%M:%S"
        self.countSpanTag = 0
        r = requests.get('https://citadel-miner.appspot.com/data/v1/icos/raised/USD')
        self.dictShortName = r.json()['result']
        self.strShortName = str()

    def handle_starttag(self, tag, attrs):
        if tag == "tr" :
             for attr in attrs:
                if attr[0] == "data-url":
                    self.countSpanTag = 0
                    self.isStartDataTag = True
                    self.row["web"] = attr[1]

        elif tag == "div" and self.isStartDataTag is True:
            for attr in attrs:
                if attr[0]=="class" and attr[1] == "detail-col-1 detail-col-name":
                    self.isTagName = True            
        elif tag == "span" and self.isTagName is True:
            self.isTagName = False
            self.isSpanName = True 
            self.countSpanTag = 1         
        elif tag == "td":
            if self.isEndDateComm is True:
                 self.isEndDateComm = False
                 self.isEndDate = True
            elif self.isStartDateComm is True:	 
                 self.isStartDateComm = False
                 self.isStartDate = True
            for attr in attrs:
                if attr[0] == "class" and attr[1] == "detail-col-descr":
                    self.isDescTd = True
                elif attr[0] == "class" and attr[1] == "text-right field-raised":
                    self.isRaisedTd = True                    
        elif tag == "p" and self.isDescTd is True:
            self.isDescTd = False
            self.isDescP = True    

    def handle_endtag(self, tag):
        if self.isStartDataTag is True and tag == "tr":
            self.isStartDataTag = False
            self.row["today"] = datetime.datetime.utcnow()
            self.row["Source"] = "https://www.smithandcrown.com/icos/"
            self.dataTable.addRow(self.row)
            self.ind = self.ind + 1
            self.row.clear()
    
    def handle_data(self, data):
# trimming of data string
        data = data.lstrip()
        data = data.rstrip()
# field "Name"
        if self.countSpanTag == 1:
            self.countSpanTag = 2
            self.isSpanName = False
            self.row["Name"] = data
        elif self.countSpanTag == 2:
            self.row["Name"] = self.row["Name"] + data
            self.countSpanTag = 0
            self.strShortName = data.replace("(", "").replace(")", "")
        elif self.isDescP is True:
            self.isDescP = False
            self.row["Description"] = data
        elif self.isRaisedTd is True and self.isRaisedComm is True:
            self.isRaisedTd = False
            self.isRaisedComm = False
# deleting symbol from data of Raised-value
            data = data.replace("$", "").replace("--", "0")
            if self.strShortName not in self.dictShortName.keys():
                self.row["Raised"] = data
            else:
                self.row["Raised"] = str(self.dictShortName[self.strShortName])    
      
        elif self.isStartDate is True:
            self.isStartDate = False
            try:
            	self.row["Start"] =  datetime.datetime.strptime(data, self.format1)
            except:
                self.row["Start"] =  datetime.datetime.utcnow()
                
        elif self.isEndDate is True:
            self.isEndDate = False
            
            try:
                self.row["End"] = datetime.datetime.strptime(data, self.format1) 
            except:
                self.row["End"] = datetime.datetime.strptime("00:00:00", self.format2)
 # detection tag of "Start" and "End"
    def handle_comment(self, data):
        if data == " Start Date ":
            self.isStartDateComm = True
        if data == " End Date ":
            self.isEndDateComm = True
        if data == " Raised ":
            self.isRaisedComm = True
        
# class of parser https://icotracker.net/current  
class IcoTrackerParser(HTMLParser):
    def __init__(self, dataTable):
        HTMLParser.__init__(self)
        self.dataTable = dataTable
        self.ind = 0
#creat
        self.row = dict()
        self.isStartTag = False
        self.isName = False
        self.isDescrTag = False
        self.isFirstDescr = True
        self.isWebTag = False
        self.countWebTag = 0
        self.countTextBlack = 0
        self.isRaisedTag = False
        self.stackDiv = list()
        self.format1 = "%d/%m/%Y"
# proccessing of start tag    
    def handle_starttag(self, tag, attrs):
        if tag == "div":
            self.stackDiv.append(("div", None))
            for attr in attrs:
                if attr[0] == "class" and attr[1] == "card-block":
                    self.countWebTag = 0
                    self.countTextBlack = 0
                    self.isFirstDescr = True
                    self.stackDiv[len(self.stackDiv) -1] = ("div", "card-block")
                if attr[0] == "class" and attr[1] == "cp-col-sm col-12":
                    self.isDescrTag = True
        elif tag == "a":
            for attr in attrs:        
                if attr[0] == "title" and attr[1] == "Project Details":
                    self.isName = True
        elif tag == "span":
            for attr in attrs:
                if attr[0] == "class" and attr[1] == "td-u":
                    self.isWebTag = True
                elif attr[0] == "class" and attr[1] == "text-black":
                    self.isRaisedTag = True  
# proccessing of end-tag    
    def handle_endtag(self, tag):
        if tag == "div":
            if self.stackDiv.pop()[1] == "card-block":
# setting value into field "today"
                self.row["today"] = datetime.datetime.utcnow()
# setting value into field "Source"
                self.row["Source"] = "https://icotracker.net/current"
                self.dataTable.addRow(self.row)
        return ""
# proccessing of content-tag   
    def handle_data(self, data):
        data = data.rstrip()
        data = data.lstrip()
# if tag match field "Name"
        if self.isName is True:
            self.isName = False
            self.row["Name"] = data
# if tag match field "Description"
        elif self.isDescrTag is True:
            self.isDescrTag = False
            if self.isFirstDescr is True:
                self.isFirstDescr = False
                self.row["Description"] = data
# if tag match field "Web"
        elif self.isWebTag is True:           
            self.isWebTag = False
            self.countWebTag = self.countWebTag + 1
            if self.countWebTag == 2:
                self.row["web"] = data
# if tag match field "Raised"
        elif self.isRaisedTag is True:
            self.isRaisedTag = False
            try:
                data = data.replace(",", "")
                self.row["Raised"] = data
            except:
                self.row["Raised"] = 0
            self.countTextBlack = self.countTextBlack + 1
           
            strRangeTime = data.split(" ")
            try:
                self.row["Start"] =  datetime.datetime.strptime(strRangeTime[0], self.format1)
            except:
                a = 1
            try:
                self.row["End"] = datetime.datetime.strptime(strRangeTime[4], self.format1)
            except:
                a = 1
#the table object of table 
dataTable = DataTable()
 # create object of parser
iParser = IcoTrackerParser(dataTable)
l = urlopen("https://icotracker.net/current")
s = l.read()
# deleting unhandled symbol from response
s = s.decode("utf-8")
s = s.replace("<text>", "")
s = s.replace("</text>", "")
s = s.replace(u"\u0243", "")
# parsing of response object "icotracker
iParser.feed(s)
#create object of parser
p =MyHTMLParser(dataTable)
l = urlopen('https://www.smithandcrown.com/icos/')
s = l.read()

# parsing of response object "smithandcrown.com"
p.feed(s.decode("utf-8"))
dayRange = datetime.timedelta(14)
dayRangeAll = datetime.timedelta(30)
format_date = '%Y-%m-%d'
raised_variable = 1
#writting data into csv-file
with open('data.csv', 'w') as csvfile:
    fieldnames = ['today', 'Name', 'web', 'Description', 'Start', 'End', 'Raised', 'Source']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for ind in range(0, dataTable.getSize()):
        try:
            strDesc = dataTable.getRow(ind)['Description']
        except:
            strDesc = ""
        end = dataTable.getRow(ind)['End']
        start = dataTable.getRow(ind)['Start']
        today = dataTable.getRow(ind)['today']
        strDesc = strDesc.upper()
        try:
            strRaised = dataTable.getRow(ind)['Raised'].replace(",", "")
            raised = int(float(strRaised))
        except:
            raised = 0
        if  strDesc.find('SCAM') != -1:
            continue
        if end < today + dayRange:
            continue
        if end - start < dayRangeAll:
            continue 
        if raised >= raised_variable or start <= today - dayRange:  
            writer.writerow({'today': today.strftime(format_date), 'Name': dataTable.getRow(ind)['Name'], 'web': dataTable.getRow(ind)['web'], 'Description': strDesc.replace(u"\u2019", "'"),\
            'Start': start.strftime(format_date),'End': end.strftime(format_date), 'Raised': str(raised), 'Source': dataTable.getRow(ind)['Source']})
   
