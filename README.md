# Парсер сайта eda.ru

#### Ссылки
* сайт http://eda.ru
* ссылка на страницу рецептов http://eda.ru/recepty/page2
* ссылка на страницу рецепта http://eda.ru/recepty/zakuski/-80354
* Задание https://docs.google.com/document/d/1JzPgwqfxMfm7x7EhoIIzs7O1DQVYXjmF5Qhb3qIB7GY/edit?usp=sharing
* ТЗ https://gist.github.com/tensie/b5c7adf174466a7f226d#Технические-ограничения
* ТЗ по немецкому сайту: https://docs.google.com/document/d/1pPWZHIUDmmYKamQJFAuLLe4zkDENeiUssEgkdzkz2hE/edit?usp=sharing

#### Документация
* http://docs.grablib.org/ru/latest/grab/tutorial.html
* https://habrahabr.ru/post/127584/
* https://github.com/kslazarev/data-mining-example
* https://habrahabr.ru/post/280238/
* http://www.pythonforbeginners.com/python-on-the-web/web-scraping-with-beautifulsoup/
* http://wiki.python.su/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%B8/BeautifulSoup